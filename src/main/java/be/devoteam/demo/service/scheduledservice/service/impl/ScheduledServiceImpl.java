package be.devoteam.demo.service.scheduledservice.service.impl;

import be.devoteam.demo.service.scheduledservice.client.RandomFeignClient;
import be.devoteam.demo.service.scheduledservice.service.ScheduledService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ScheduledServiceImpl implements ScheduledService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RandomFeignClient randomFeignClient;

    @Scheduled(fixedDelay = 5000L)
    @Override
    public void executeScheduledTask() {
        logger.info("Executing scheduled task");
        try{
            randomFeignClient.callRandomService();
        } catch (Exception e) {
            logger.error("An exception occured", e);
        }
        logger.info("Finished scheduled task");
    }
}
