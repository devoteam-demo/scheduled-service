package be.devoteam.demo.service.scheduledservice.service;

public interface ScheduledService {

    void executeScheduledTask();

}
